/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ms_demo_item.controller;
import com.example.ms_demo_item.model.ProductItem;
import com.example.ms_demo_item.service.ProductItemService;
import java.math.BigDecimal;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author infan
 */

@RestController
public class ProductItemController {
    
    @Autowired
    @Qualifier("serviceFeign")
    private ProductItemService productItemService;
    
    @GetMapping("getProductItems")
    public List<ProductItem> getProductItems(){
        return productItemService.getProductItems();
    }
    
    @GetMapping("getProductItem/{id}/quantity/{quantity}")
    public ProductItem getProductItem(@PathVariable Long id, @PathVariable BigDecimal quantity){
        return productItemService.getProductItem(id, quantity);
    }
}
