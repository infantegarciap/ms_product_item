/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ms_demo_item.model;

import java.math.BigDecimal;

/**
 *
 * @author infan
 */
public class ProductItem {
    private Product product;
    private BigDecimal quantity;

    public ProductItem() {
    }

    public ProductItem(Product product, BigDecimal quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
    
    public BigDecimal getTotal(){
        return product.getPrice().multiply(quantity);
    }
}
