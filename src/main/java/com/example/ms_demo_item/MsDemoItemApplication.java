package com.example.ms_demo_item;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class MsDemoItemApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsDemoItemApplication.class, args);
	}

}
