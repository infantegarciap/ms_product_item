/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ms_demo_item.client;

import com.example.ms_demo_item.model.Product;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 *
 * @author infan
 * Implementación de un rest usando feign client
 */
@FeignClient(name = "servicio-producto", url = "localhost:8001")
public interface ProductClientRest {
    
    @GetMapping("/getProducts")
    public List<Product> getProducts();
  
    @GetMapping("/getProduct/{id}")
    public Product getProduct(@PathVariable Long id);
}
