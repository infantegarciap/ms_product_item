/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ms_demo_item.service;

import com.example.ms_demo_item.model.Product;
import com.example.ms_demo_item.model.ProductItem;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author infan
 */
@Service("serviceRestTemplate")
public class ProductItemServiceImpl implements ProductItemService {

    @Autowired
    private RestTemplate productRest;

    @Override
    public List<ProductItem> getProductItems() {
        //Comunicación con el microservicio
        List<Product> products = Arrays.asList(productRest.getForObject("http://localhost:8001/getProducts", Product[].class));
        return products.stream().map(p -> new ProductItem(p, BigDecimal.ONE)).collect(Collectors.toList());
    }

    @Override
    public ProductItem getProductItem(Long productItemId, BigDecimal quantity) {
        //Mapeo el parametro de la url
        Map<String, String> pathParam = new HashMap<>();
        pathParam.put("id", productItemId.toString());
        //Comunicación con el microservicio
        Product product = productRest.getForObject("http://localhost:8001/getProduct/{id}", Product.class, pathParam);
        return new ProductItem(product, quantity);
    }

}
