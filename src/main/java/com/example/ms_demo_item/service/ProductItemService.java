/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ms_demo_item.service;

import com.example.ms_demo_item.model.ProductItem;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author infan
 */
public interface ProductItemService {
    public List<ProductItem> getProductItems();
    public ProductItem getProductItem(Long productItemId, BigDecimal quantity);
}
