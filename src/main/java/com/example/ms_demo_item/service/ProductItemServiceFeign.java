/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ms_demo_item.service;

import com.example.ms_demo_item.client.ProductClientRest;
import com.example.ms_demo_item.model.ProductItem;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author infan
 */
@Service("serviceFeign")
public class ProductItemServiceFeign implements ProductItemService {

    @Autowired
    private ProductClientRest clientFeign;

    @Override
    public List<ProductItem> getProductItems() {
       return clientFeign.getProducts().stream().map(p -> new ProductItem(p, BigDecimal.ONE)).collect(Collectors.toList());
    }

    @Override
    public ProductItem getProductItem(Long productItemId, BigDecimal quantity) {
        return new ProductItem(clientFeign.getProduct(productItemId), quantity);
    }

}
