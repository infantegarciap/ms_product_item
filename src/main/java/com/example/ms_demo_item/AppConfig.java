/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ms_demo_item;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author infan
 */
@Configuration
public class AppConfig {

    @Bean("productRest")
    public RestTemplate restTemplateRegister() {
        return new RestTemplate();
    }
}
